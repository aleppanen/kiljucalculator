/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.IModel;
import Model.Model;
import Model.Sugar;
import View.IView;
import java.text.DecimalFormat;
import java.util.List;
import javax.swing.JFrame;

/**
 *
 * @author aatu
 */
public class Controller {
    
    IModel model;
    IView view;
    
    private DecimalFormat threeDecimalFormat;
    private DecimalFormat twoDecimalFormat;
    
    public Controller(IView frame){
        view = frame;
        model = new Model();
        threeDecimalFormat = new DecimalFormat("0.000");
        twoDecimalFormat = new DecimalFormat("0.00");
    }
    
    public void setLiquidVolume(Double volume){
        model.setLiquidVolume(volume);
    }
    
    public void setSugarList(List<Sugar> list){
        model.setSugarList(list);
    }
    
    public void setYeastPrice(double price){
        model.setYeastPrice(price);
    }
    
    public void Calculate(){
        view.getAlcoholField().setText(String.valueOf(twoDecimalFormat.format(model.getAlcohol())));
        view.getGravityField().setText(String.valueOf(threeDecimalFormat.format(model.getGravity())));
        //price shouldnt be a double but it's not so important in this case
        view.getTotalPriceField().setText(String.valueOf(twoDecimalFormat.format(model.getPrice())));
        view.getLiterPriceField().setText(String.valueOf(twoDecimalFormat.format(model.getAlcoholPrice())));

        
    }
    
}

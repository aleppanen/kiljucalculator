/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

/**
 * Interface used by the controller
 * @author aatu
 */
public interface IView {
    public javax.swing.JLabel getAlcoholField();
    public javax.swing.JLabel getGravityField();
    public javax.swing.JLabel getTotalPriceField();
    public javax.swing.JLabel getLiterPriceField();
}

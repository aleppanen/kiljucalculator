/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Kilju ingredients
 * @author aatu
 */
public class Kilju {
    private double liquidVolume;    
    private List<Sugar> sugars;
    private double yeastPrice;
    
    public Kilju(){
        
    }
    
    public double getTotalSugarGrams(){
        double total = 0;
        
        for (Sugar sugar : sugars){
            total = total + sugar.getAmount()*(sugar.getSugarPercentage()/100);
        }
        return total;
    }
    
    public double getTotalPrice(){
        double total = 0;
        
        for (Sugar sugar : sugars){
            total = total + (sugar.getKgPrice() * sugar.getAmount() / 1000);
        }
        total = total + yeastPrice;
        return total;
    }

    public void setSugars(List<Sugar> sugars) {
        this.sugars = sugars;
    }

    public void setLiquidVolume(double liquidVolume) {
        this.liquidVolume = liquidVolume;
    }

    public double getLiquidVolume() {
        return liquidVolume;
    }

    public void setYeastPrice(double yeastPrice) {
        this.yeastPrice = yeastPrice;
    }
    
    
    
    
    
    
    
    
    
}

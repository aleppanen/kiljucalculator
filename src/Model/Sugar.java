/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.text.DecimalFormat;

/**
 * Information about any sugary products in the sugar wine (sugar, syrup, fruits etc.)
 * 
 * @author aatu
 */
public class Sugar {
    private String name;
    private double sugarPercentage;
    private double amount;
    private double kgPrice;
    private DecimalFormat priceFormat;

    public Sugar(String name, double sugarPercentage, double amount, double kgPrice) {
        this.name = name;
        this.sugarPercentage = sugarPercentage;
        this.amount = amount;
        this.kgPrice = kgPrice;
        priceFormat = new DecimalFormat("0.00");
    }
    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSugarPercentage() {
        return sugarPercentage;
    }

    public void setSugarPercentage(double sugarPercentage) {
        this.sugarPercentage = sugarPercentage;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getKgPrice() {
        return kgPrice;
    }

    @Override
    public String toString() {
        return name + ", " + amount + "g, " + priceFormat.format(kgPrice * (amount/1000)) + "€";
    }
    
    
            
    
    
}

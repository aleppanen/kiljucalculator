/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.List;

/**
 * Model interface
 * @author aatu
 */
public interface IModel {
    
    public double getGravity();
    public double getPrice();
    public double getAlcohol();
    public double getAlcoholPrice();
    
    public void setYeastPrice(Double price);
    public void setLiquidVolume(Double volume);
    public void setSugarList(List<Sugar> list);
    
}

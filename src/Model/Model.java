/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Most calculations done here
 * @author aatu
 */
public class Model implements IModel{
    
    Kilju kilju;
    
    public Model(){
        kilju = new Kilju();
    }
    
    @Override
    public double getGravity(){
        
        double sugarGramsPerLiter = kilju.getTotalSugarGrams()/kilju.getLiquidVolume();
        double sugarVolumePerLiter = sugarGramsPerLiter/1.59;
        double waterVolumePerLiter = 1000 - sugarVolumePerLiter;
        double totalWeightOfLiter = waterVolumePerLiter + sugarVolumePerLiter * 1.59;
        //Assuming temperature is 20 degrees celcius
        double specificGravity = (totalWeightOfLiter/1000) / 0.99821;
        
        return specificGravity;
        
    }

    @Override
    public double getPrice() {
        return kilju.getTotalPrice();
    }

    @Override
    public double getAlcohol() {
        return (getGravity() - 0.995)/0.0074;
    }

    @Override
    public double getAlcoholPrice() {
        return getPrice()/(kilju.getLiquidVolume()*(getAlcohol() / 100));
    }

    @Override
    public void setLiquidVolume(Double volume) {
        kilju.setLiquidVolume(volume);
    }

    @Override
    public void setSugarList(List<Sugar> list) {
        kilju.setSugars(list);
    }

    @Override
    public void setYeastPrice(Double price) {
        kilju.setYeastPrice(price);
    }
    
}
